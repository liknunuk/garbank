<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Bangsam extends Controller
{
    // cek session login
    public function __construct(){
        if(isset($_SESSION['utype']) && isset($_SESSION['uip'])){
            if( $_SESSION['utype'] == 'management' && $_SESSION['uip'] == 'Bangsam'){
                $permitted = true;
            }else{
                $permitted = false;
            }
        }

        if( $permitted == false ){
            header("Location:".BASEURL."Home/kepareng");
        }
    }
    
    // method default
    public function index(){
        $data['title'] = "Bank Sampah - Beranda";
        $this->view('template/headersb',$data);

        $data['akuSaldo'] = $this->model('Model_bangsam')->totalSaldo();
        $data['akuNasab'] = $this->model('Model_bangsam')->nasabahAktif();
        
        $this->view('bangsam/index',$data);
        $this->view('template/footersb');
    }

    // form input nasabah
    public function frNasabah($mod = "baru",$nmRek=""){
        $data['title'] = "Bank Sampah - Nasabah";
        $this->view('template/headersb',$data);
        if($nmRek !=""){
            $data['frn_nmRekening'] = $nmRek;
            $data['nasabah'] = $this->model('Model_bangsam')->dataNasabah($data);
        }else{
            $data['nasabah'] = null;
        }
        $data['kecamatan'] = $this->model('Model_bangsam')->dtKecamatan();
        $data['transbank'] = $this->model('Model_bangsam')->bankList();
        $data['mod'] = $mod;
        $this->view('bangsam/bsFrNasabah',$data);
        $this->view('template/footersb');
    }

    // daftar nasabah
    public function dfNasabah($pn = 1){
        $data['title'] = "Bank Sampah - Nasabah";
        $this->view('template/headersb',$data);
        $data['hal'] = $pn;
        $data['nasabah'] = $this->model('Model_bangsam')->listNasabah();
        $this->view('bangsam/bsDfNasabah',$data);
        $this->view('template/footersb');
    }

    // data nasabah
    public function dtNasabah($nmRek){
        $data['title'] = "Bank Sampah - Nasabah";
        $this->view('template/headersb',$data);
        $data['frn_nmRekening'] = $nmRek;
        $data['nasabah'] = $this->model('Model_bangsam')->dataNasabah($data);
        $this->view('bangsam/bsDtNasabah',$data);
        $this->view('template/footersb');
    }

    // simpan data nasabah
    public function setNasabah(){
        if($_POST['mod']=='baru'){
            //echo "nasabah baru";
            if( $this->model('Model_bangsam')->tambahNasabah($_POST) > 0 ){
                Alert::set('Data nasabah','berhasil','ditambahkan','success');
                header("Location:".BASEURL."Bangsam/dfNasabah");
            }else{
                Alert::set('Data nasabah','gagal','ditambahkan','danger');
                header("Location:".BASEURL."Bangsam/dfNasabah");
            }
        }elseif($_POST['mod']=='liru'){
            if( $this->model('Model_bangsam')->updateNasabah($_POST) > 0 ){
                Alert::set('Data nasabah','berhasil','diubah','info');
                header("Location:".BASEURL."Bangsam/dfNasabah");
            }else{
                Alert::set('Data nasabah','gagal','diubah','danger');
                header("Location:".BASEURL."Bangsam/dfNasabah");
            }
        }
    }

    // form user management
    public function frMgmt($mod = "baru",$uid=""){
        $data['title'] = "Bank Sampah - Manajemen";
        $this->view('template/headersb',$data);
        $data['mod'] = $mod;
        if( $uid != "" ){
            $data['mgmt'] = $this->model('Model_bangsam')->dataUser($uid);
        }else{ $data['mgmt'] = NULL; }
        $this->view('bangsam/bsFrMgmt',$data);
        $this->view('template/footersb');
    }


    // form password user management
    public function frMgmtPass($uid=""){
        $data['title'] = "Bank Sampah - Manajemen";
        $this->view('template/headersb',$data);
        $data['uid'] = $uid;
        $this->view('bangsam/bsFrMgmtPass',$data);
        $this->view('template/footersb');
    }

    // reset password user management
    public function pwdrst($uid){        
        if( $this->model('Model_bangsam')->pwdrst($uid) > 0 ){
            Alert::set('Password' , 'berhasil' , 'direset' , 'info');
            header("Location:".BASEURL."Bangsam/dfMgmt");
        }else{
            Alert::set('Password' , 'gagal' , 'direset' , 'warning');
            header("Location:".BASEURL."Bangsam/dfMgmt");
        }
    }

    // daftar user management
    public function dfMgmt($pn = 1){
        $data['title'] = "Bank Sampah - Management";
        $this->view('template/headersb',$data);
        $data['hal'] = $pn;
        $data['staff'] = $this->model('Model_bangsam')->listUser();
        $this->view('bangsam/bsDfMgmt',$data);
        $this->view('template/footersb');
    }


    // simpan data Management
    public function setMgmt(){
        // print_r($_POST);
        if($_POST['mod']=='baru'){
            if( $this->model('Model_bangsam')->tambahUser($_POST) > 0 ){
                Alert::set('Data staff','berhasil','ditambahkan','success');
                header("Location:".BASEURL."Bangsam/dfMgmt");
            }else{
                Alert::set('Data staff','gagal','ditambahkan','danger');
                header("Location:".BASEURL."Bangsam/dfMgmt");
            }
        }elseif($_POST['mod']=='liru'){
            if( $this->model('Model_bangsam')->updateUser($_POST) > 0 ){
                Alert::set('Data staff','berhasil','dimutakhirkan','info');
                header("Location:".BASEURL."Bangsam/dfMgmt");
            }else{
                Alert::set('Data staff','gagal','dimutakhirkan','danger');
                header("Location:".BASEURL."Bangsam/dfMgmt");
            }
        }
    }

    public function retire($uid){
        
        if($this->model('Model_bangsam')->deactivateUser($uid) > 0 ){
            Alert::set('Staff' , 'sudah' , 'dipensiunkan' , 'info');
            header("Location:".BASEURL."Bangsam/dfMgmt");
        }
    }

    public function notire(){
        Alert::set('Staff' , 'batal' , 'dipensiunkan' , 'info');
        header("Location:".BASEURL."Bangsam/dfMgmt");
    }

    // form login 
    public function login(){
        $data['title'] = "Bank Sampah - Login";
        teller/
        $this->view('bangsam/bsFrLogin',$data);
    }

    public function userAuth($data){
        $login = $this->model('Model_bangsam')->userAuth($data);
        if($login == NULL){
            header("Location:" . BASEURL . "Bangsam/login");
            $_SESSION['uauth'] = "User tidak terdaftar!";
        }else{
            $_SESSION['mgmtUser'] = $login['mgmtName'];
            header("Location:" . BASEURL . "Bangsam/");
        }
    }

    public function nmRekBaru($prefik){
        $nomor = $this->model('Model_bangsam')->nmRekBaru($prefik);
        if( $nomor == NULL ){
            echo $prefik . '00001';
        }else{
            $nmBaru = $nomor['nomorTerakhir'] + 1;
            echo $prefik . sprintf("%05d",$nmBaru);
        }
    }
}
