<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    // membambil dara dari model Model
    
    // $data['xxx'] = $this->model('Model_xxx')->method();
    // variabel untuk title bar
    // $data['title']="Bank Sampah";
    // $this->view('template/header',$data);
    // $this->view('template/navbar');
    // mengirim $data ke body
    // $this->view('home/index',$data);
    // // bodh halaman
    // $this->view('template/footer');
    error_reporting(0);
    $this->view('home/login');
    
  }

  public function authuser(){
    $authData = $this->model('Model_user')->authuser($_POST);
    
    if( $authData['tipe'] == 'nasabah'){
      
      $_SESSION['utype'] = 'nasabah';
      $_SESSION['uid']   = $authData['data']['nomorRekening'];
      $_SESSION['name']   = $authData['data']['namaNasabah'];
      $_SESSION['uip']   = 'Nasab';
      header("Location: " . BASEURL . $_SESSION['uip']);

    }elseif( $authData['tipe'] == 'management'){

      $_SESSION['utype'] = 'management';
      $_SESSION['name']   = $authData['data']['mgmtName'];
      $_SESSION['uid']   = $authData['data']['userId'];
      $_SESSION['uip']   = ucfirst($authData['data']['mgmtLevel']);
      header("Location: " . BASEURL . $_SESSION['uip']);

    }else{

      $uip = $authData['data'];
      if(!isset($_SESSION['ip']['$uip'])) $_SESSION['ip']['$uip'] = 0;
      if( $_SESSION['ip'][$uip] >= 3 ) $_SESSION['blocked'] = true;
      $_SESSION['ip'][$uip] += 1;
      Alert::set('Login Ilegal dari',$uip,'ke-'.$_SESSION['ip'][$uip],'warning');
      header("Location:".BASEURL);

    }
  }

  public function kepareng(){
    session_unset();
    session_destroy();
    header("Location: " . BASEURL );
  }

}
