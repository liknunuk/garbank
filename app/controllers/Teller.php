<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Teller extends Controller
{
  // cek session login
  public function __construct(){
    if(isset($_SESSION['utype']) && isset($_SESSION['uip'])){
        if( $_SESSION['utype'] == 'management' && $_SESSION['uip'] == 'Teller'){
            $permitted = true;
        }else{
            $permitted = false;
        }
    }

    if( $permitted == false ){
        header("Location:".BASEURL."Home/kepareng");
    }
  }
  
  // data transaksi
  public function index()
  {
    $data['title'] = "Bank Sampah - Transaksi";
    $this->view('template/mgt-navbar',$data);
    $this->view('template/header',$data);
    $data['toptrx'] = $this->model('Model_teller')->last40trx();
    $data['rqtrfr'] = $this->model('Model_teller')->cekRqTrx();
    $this->view('teller/index',$data);
    $this->view('template/footer');
  }

  // input transaksi
  public function trx($accnum = ''){
    $data['title'] = "Bank Sampah - Transaksi";
    $this->view('template/header',$data);
    $data['accNum'] = $accnum;
    $nasabah['frn_nmRekening'] = $accnum;
    $data['trxHistory'] = $this->model('Model_teller')->trxHistory($accnum);
    $data['nasabah'] = $this->model('Model_bangsam')->dataNasabah($nasabah);
    $this->view('teller/tlFrTrx',$data);
    $this->view('template/footer');
  }

  public function setTrx(){
    if ( $this->model('Model_teller')->trxRecord($_POST) > 0 ){
      Alert::set('Transaksi','berhasil','dicatat','success');
      header("Location:".BASEURL."Teller");
    }
  }

  public function setEtfr($idx){
    if( $this->model('Model_teller')->setEtfr($idx) > 0 ){
      Alert::set('Permintaan Transfer','Berhasil','Dicatat');
      header("Location:" . BASEURL . "Teller/");
    }
    
  }

  public function login(){
      $this->view('teller/tlFrLogin');
  }

  public function updpass(){
    $data['title'] = "Bank Sampah - Password";
    $this->view('template/mgt-navbar',$data);
    $this->view('template/header',$data);
    $this->view('bangsam/bsFrMgmtPass',$data);
    $this->view('template/footer');    
  }

  public function setMgmtPass(){
    
    if( $this->model('Model_teller')->setNewPassword($_POST) >  0 ){
      Alert::set('Password','Berhasil','Diganti','info');
      header("Location:".BASEURL."Teller");
    }else{
      Alert::set('Password','Gagal','Diganti','danger');
      header("Location:".BASEURL."Teller");
    }
    
  }

}
