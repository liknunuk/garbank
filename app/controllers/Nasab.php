<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Nasab extends Controller
{
    
  
  // cek session login
  public function __construct(){
    if(isset($_SESSION['utype']) && isset($_SESSION['uip'])){
        if( $_SESSION['utype'] == 'nasabah' && $_SESSION['uip'] == 'Nasab'){
            $permitted = true;
        }else{
            $permitted = false;
        }
    }

    if( $permitted == false ){
        header("Location:".BASEURL."Home/kepareng");
    }
  }
    // method default
  public function index($pn = 1)
  {
    $data['title'] = "Bank Sampah - Nasabah";
    $this->view('template/header',$data);
    $this->view('template/navbar');

    $data['nasabah'] = $this->model('Model_bangsam')->dataNasabah($_SESSION);
    $data['tabunga'] = $this->model('Model_nasabah')->cekSaving($_SESSION['uid'],$pn);
    $data['kodetrx'] = $this->model('Model_nasabah')->kodeTrx();
    $this->view('nasab/index',$data);
    $this->view('template/footer');

  }

  public function rqmut(){
    $data['title'] = "Bank Sampah - Transfer Eksternal";
    $this->view('template/header',$data);
    $this->view('template/navbar');

    $data['saldo'] = $this->model('Model_nasabah')->saldoAkhir($_SESSION['uid']);
    $data['rekno'] = $_SESSION['uid'];
    // $data['kodetrx'] = $this->model('Model_nasabah')->kodeTrx();

    $this->view('nasab/ns_rqeTfr',$data);
    $this->view('template/footer');
  }

  public function reqETrf(){
      /*
      Array ( [ns_balance] => 45.000,00 [ns_nmRekening] => 117-19-00002 [ns_nominal] => 15000 [ns_submit] => Kirim ) 
      */
      if( $this->model('Model_nasabah')->rqTrx($_POST['ns_nmRekening'] , $_POST['ns_nominal']) > 0){
          Alert::set('Permintan Transfer' , 'Berhasil' , 'Dicatat' , 'success');
          header("Location:" . BASEURL . "Nasab/");
      }
  }
}
