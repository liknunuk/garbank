<?php
// sesuaikan nama kelas, tetap extends ke Controller
class BankData extends Controller
{
    public function index(){
        $this->corsSetup();
        echo "";
    }

    public function tl_carinas($keyword){
       $this->corsSetup();
       $data['tlr_namasabah'] = $keyword;
       $nasabah = $this->model('Model_teller')->cariNasab($data);
       echo json_encode($nasabah);
    }

    public function trxId(){
        $this->corsSetup();
        $trxId = $this->model('Model_teller')->trxId();
        echo $trxId;
    }

    private function corsSetup(){
        header("Access-Control-Allow-Origin: *");
    }
}