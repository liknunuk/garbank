<?php

class Alert
{
    public static function set($objek, $pesan , $operasi , $tipe){

        $_SESSION['alert'] = [
            'objek'  => $objek,
            'pesan'  => $pesan,
            'operasi'=> $operasi,
            'warna'  => $tipe
        ];
    }
    
    public static function show(){
        if ( isset($_SESSION['alert'])){
            
            echo '
            <div class="alert alert-'.$_SESSION['alert']['warna'].' alert-dismissible fade show" role="alert">
                ' . $_SESSION['alert']['objek'] . ' <strong>'.$_SESSION['alert']['pesan'].'</strong> ' . $_SESSION['alert']['operasi'] . '
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            ';

            unset($_SESSION['alert']);
            
        }
    }
}