<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="bg-secondary text-center text-light">
        <ul class="list-inline" style="font-size: 14px;">
            <li class="list-inline-item">Nomor Rekening: <?=$data['nasabah']['nomorRekening'];?></li>
            <li class="list-inline-item">Nama Nasabah: <?=$data['nasabah']['namaNasabah'];?></li>
            <li class="list-inline-item">Alamat: RT/RW <?=$data['nasabah']['rtrw'] ." Desa/Kel. ". $data['nasabah']['desa'] ;?></li>
            <li class="list-inline-item">Nomor Telp. : <?=$data['nasabah']['nomorTelp'];?></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-lg-12">
        <?php Alert::show(); ?>
    </div>
  </div>
  <div class="row mt-3">
    <div class="col-lg-8">
        <div class="card">
          <div class="card-header bg-primary text-light">
            <p class="h4">Catatan Tabungan</p>
          </div>
          <div class="card-body table-responsive">
            <table class="table table-sm table-striped">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Kode</th>
                  <th>Tanggal</th>
                  <th>Jumlah</th>
                  <th>Saldo</th>
                </tr>
              </thead>
              <tbody style="font-size:10pt;">
                <?php 
                $baris = count($data['tabunga']);
                foreach($data['tabunga'] AS $tabungan ): ?>
                  <tr>
                    <td><?=$baris;?></td>
                    <td><?=$tabungan['kdTrx'];?></td>
                    <td><?=olahTanggal($tabungan['tanggal']);?></td>
                    <td class="text-right"><?=number_format($tabungan['nominal'],2,',','.');?></td>
                    <td class="text-right"><?=number_format($tabungan['saldo'],2,',','.');?></td>
                  </tr>
                <?php
                $baris-=1; 
                endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>
    <div class="col-lg-4">
      <div class="card">
        <div class="card-header bg-primary text-light">
          <p class="h4">Kode Transaksi</p>
        </div>
        <div class="card-body table-responsive">
          <table class="table table-sm table-striped">
            <thead>
              <tr>
                <th>Kode</th>
                <th>Arti</th>
              </tr>
            </thead>
            <tbody style="font-size:10pt;">
                <?php foreach($data['kodetrx'] AS $ktrx): ?>
                    <tr>
                    <td><?=$ktrx['kode'];?></td>
                    <td><?=$ktrx['arti'];?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->view('template/bs4js'); ?>

<?php
function olahTanggal($tgjam){
    list($tgl,$jam) = explode(" " , $tgjam );
    list($t,$b,$h)  = explode("-" , $tgl);
    return "{$h}/{$b}/{$t}";
}
?>