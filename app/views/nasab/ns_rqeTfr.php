<div class="container">
  <div class="row">
    <div class="col-lg-12 card">
      <div class="card-header text-center">
        <p class='h4'>Permohonan Transfer</p>
      </div>
      <form action="<?=BASEURL;?>Nasab/reqETrf" method="post">

        <div class="form-group row">
          <label for="ns_balance" class="col-sm-3">Saldo Akhir</label>
          <div class="col-sm-9">
            <input type="text" name="ns_balance" id="ns_balance" class="form-control" value="<?=number_format($data['saldo']['saldo'],2,',','.');?>" readonly >
          </div>
        </div>        

        <div class="form-group row">
          <label for="ns_nmRekening" class="col-sm-3">Nomor Rekening</label>
          <div class="col-sm-9">
            <input type="text" name="ns_nmRekening" id="ns_nmRekening" class="form-control" value="<?=$data['rekno'];?>" readonly >
          </div>
        </div>

        <div class="form-group row">
          <label for="ns_nominal" class="col-sm-3">Jumlah</label>
          <div class="col-sm-9">
            <input type="number" name="ns_nominal" id="ns_nominal" class="form-control" min='10000' value="10000">
          </div>
        </div>

        <?php
          if($data['saldo']['saldo'] < 20000 ){
            echo '
            <div class="form-group row">
              <button class="btn btn-danger form-control" disabled>Saldo Kurang dari Rp 20.000,00</button>
            </div>
            ';      
          }else{
              echo '
                <div class="form-group row">
                    <label for="ns_submit" class="col-sm-3 bg-danger text-dark">Cek Data</label>
                    <div class="col-sm-9">
                    <input type="submit" name="ns_submit" id="ns_submit" class="form-control btn btn-primary" value="Kirim">
                    </div>
                </div>
              ';
          }
        ?>
      </form>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>