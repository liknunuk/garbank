<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
        <div class="card">
          <div class="card-header bg-success">
            <h3>DATA NASABAH</h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
                
                <table class="table table-striped table-sm">
                  <tbody>
                    <tr>
                        <td>Nomor Rekening</td>
                        <td><?=$data['nasabah']['nomorRekening'];?></td>
                    </tr>
                    <tr>
                        <td>Nama Nasabah</td>
                        <td><?=$data['nasabah']['namaNasabah'];?></td>
                    </tr>
                    <tr>
                        <td>Nomor KTP</td>
                        <td><?=$data['nasabah']['nomorKTP'];?></td>
                    </tr>
                    <tr>
                        <td>RT / RW</td>
                        <td><?=$data['nasabah']['rtrw'];?></td>
                    </tr>
                    <tr>
                        <td>Desa</td>
                        <td><?=$data['nasabah']['desa'];?></td>
                    </tr>
                    <tr>
                        <td>Nomor Telp / HP</td>
                        <td><?=$data['nasabah']['nomorTelp'];?></td>
                    </tr>
                    <tr>
                        <td>Bank Transfer</td>
                        <td><?=$data['nasabah']['namaBank'];?></td>
                    </tr>
                    <tr>
                        <td>Nomor Rekening Transfer</td>
                        <td><?=$data['nasabah']['rekeningTransfer'];?></td>
                    </tr>
                    <tr>
                        <td>Status Akun</td>
                        <td><?=$data['nasabah']['statusAktif'];?></td>
                    </tr>
                  </tbody>
                </table>
                <a class="btn btn-success" id="snb_edit" href="<?=BASEURL;?>Bangsam/frNasabah/liru/<?=$data['nasabah']['nomorRekening'];?>">Edit</a>
                <!--button class="btn btn-info" id="snb_htrx">Riwayat Transaksi</button-->
                <button class="btn btn-danger" id="snb_move">Hapus</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>