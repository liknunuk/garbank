<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
        <div class="card">
          <div class="card-header bg-success">
            <h3>DATA NASABAH</h3>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-2">
                <a class='btn btn-primary' href="<?=BASEURL;?>Bangsam/frNasabah">Nasabah Baru</a>
              </div>
              <div class="col-sm-3">
                <input type="text" id="dfn_namasabah" class="form-control">
              </div>
              <div class="col-sm-1">
                <button type="submit" class="btn btn-primary" id="dfn_nasabahseek">Cari!</button>
              </div>
              <div class="col-sm-6">
                <?php Alert::show(); ?>
              </div>
            </div>
            <div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>No. Rekening</th>
                      <th>Nama Nasabah</th>
                      <th>Alamat</th>
                      <th>Nomor KTP</th>
                      <th>Nomor Telp</th>
                      <th>Kontrol</th>
                    </tr>
                  </thead>
                  <tbody id="dataNasab">
                    <?php foreach($data['nasabah'] AS $nsb): ?>
                      <tr>
                        <td><?=$nsb['nomorRekening'];?></td>
                        <td><?=$nsb['namaNasabah'];?></td>
                        <td>RT/RW: <?=$nsb['rtrw'];?>, <?=$nsb['desa'];?></td>
                        <td><?=$nsb['nomorKTP'];?></td>
                        <td><?=$nsb['nomorTelp'];?></td>
                        <td>
                          <a href="<?=BASEURL;?>Bangsam/dtNasabah/<?=$nsb['nomorRekening'];?>">Detil</a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script src="<?=BASEURL;?>js/bsconfig.js"></script>
<script src="<?=BASEURL;?>js/bs-main.js"></script>