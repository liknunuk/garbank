<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-success">
                    <h3>NASABAH BANK SAMPAH</h3>
                </div>
                <div class="card-body">
                    <form action="<?=BASEURL;?>Bangsam/setNasabah" method="post">
                        <input type="hidden" name="mod" id="mod" value="<?=$data['mod'];?>">
                        <?php if($data['mod'] == 'baru'): ?>
                        <!-- kecamatan -->
                        <div class="form-group row">
                          <label for="frn_kecamatan" class="col-sm-2">Kecamatan</label>
                          <div class="col-sm-10">
                            <select name="frn_kecamatan" id="frn_kecamatan" class="form-control">
                              <option value="">Pilih Kecamatan Asal Nasabah</option>
                              <?php foreach($data['kecamatan'] AS $kec ): ?>
                              <option value="<?=$kec['nomorKecamatan'];?>"><?=$kec['namaKecamatan'];?></option>
                              <?php endforeach; ?>
                            </select>
                          </div>
                        </div>
                        <?php endif; ?>

                        <!-- #01 -->
                        <div class="form-group row">
                            <label for="frn_nmRekening" class="col-sm-2">Nomor Rekening</label>
                            <div class="col-sm-10">
                                <input type="text" name="frn_nmRekening" id="frn_nmRekening" class="form-control" value='<?=$data['nasabah']['nomorRekening']; ?>' readonly>
                            </div>
                        </div>

                        <!-- #02 -->
                        <div class="form-group row">
                            <label for="frn_namalenkap" class="col-sm-2">Nama Lengkap</label>
                            <div class="col-sm-10">
                                <input type="text" name="frn_namalenkap" id="frn_namalenkap" class="form-control" value="<?=$data['nasabah']['namaNasabah']; ?>">
                            </div>
                        </div>

                        <!-- #03 -->
                        <div class="form-group row">
                            <label for="frn_nik" class="col-sm-2">Nomor KTP / NIK</label>
                            <div class="col-sm-10">
                                <input type="text" name="frn_nik" id="frn_nik" class="form-control" value=<?=$data['nasabah']['nomorKTP']; ?>>
                            </div>
                        </div>

                        <!-- #04 -->
                        <div class="form-group row">
                            <label for="frn_rtrw" class="col-sm-2">RT dan RW</label>
                            <div class="col-sm-10">
                                <input type="text" name="frn_rtrw" id="frn_rtrw" class="form-control" value=<?=$data['nasabah']['rtrw']; ?>>
                            </div>
                        </div>

                        <!-- #05 -->
                        <div class="form-group row">
                            <label for="frn_desa" class="col-sm-2">Desa</label>
                            <div class="col-sm-10">
                                <input type="text" name="frn_desa" id="frn_desa" class="form-control" value=<?=$data['nasabah']['desa']; ?>>
                            </div>
                        </div>

                        <!-- #06 -->
                        <div class="form-group row">
                            <label for="frn_telp" class="col-sm-2">Nomor Telpon/HP</label>
                            <div class="col-sm-10">
                                <input type="text" name="frn_telp" id="frn_telp" class="form-control" value=<?=$data['nasabah']['nomorTelp']; ?> >
                            </div>
                        </div>

                        <!-- #07 -->
                        <div class="form-group row">
                            <label for="frn_banktrf" class="col-sm-2">Bank Transfer</label>
                            <div class="col-sm-10">
                                <select name="frn_banktrf" id="frn_banktrf" class="form-control">
                                    <?php foreach($data['transbank'] AS $tbank): ?>
                                        <option value="<?=$tbank['kodeBank'];?>" <?php if($data['nasabah'] != null && $tbank['kodeBank'] == $data['nasabah']['bankTransfer']){ echo "Selected";} ?>><?=$tbank['namaBank'];?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <!-- #08 -->
                        <div class="form-group row">
                            <label for="frn_accntrf" class="col-sm-2">No. Rekening Transfer</label>
                            <div class="col-sm-10">
                                <input type="text" name="frn_accntrf" id="frn_accntrf" class="form-control"  value=<?=$data['nasabah']['rekeningTransfer']; ?>>
                            </div>
                        </div>

                        <!-- #09 -->
                        <?php if($data['mod'] == 'baru'): ?>
                        <div class="form-group row">
                            <label for="frn_pin" class="col-sm-2">Nomor Pin</label>
                            <div class="col-sm-10">
                                <input type="text" name="frn_pin" id="frn_pin" class="form-control">
                            </div>
                        </div>
                        <?php endif; ?>

                        <!-- #10 -->
                        <div class="form-group row">
                            <label for="frn_submit" class="col-sm-2"><span class='text-danger'>Periksa Data !</span></label>
                            <div class="col-sm-10 text-right px-5">
                                <input type="submit" name="frn_submit" id="frn_submit" class="btn btn-primary" value="Simpan">
                            </div>
                        </div>

                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    let ThIni = "<?=date('y');?>";
    $("#frn_kecamatan").change( function(){
        let kdKec = $(this).val();
        let pfRek = kdKec+"-"+ThIni+"-";
        $.ajax({
            url:"<?=BASEURL;?>/Bangsam/nmRekBaru/"+pfRek,
            success: function(nmRekBaru){
                $("#frn_nmRekening").val(nmRekBaru)
            }
        })
    })
</script>