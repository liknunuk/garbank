<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-success">
                    <h3>MANAGEMENT BANK SAMPAH</h3>
                </div>
                <div class="card-body">
                <?php #print_r($_SESSION);?>
                    <form action="<?=BASEURL;?>Teller/setMgmtPass" method="post">
                        <input type="hidden" name="mgt_userId" value="<?=$_SESSION['uid'];?>">
                        
                        <!-- #01 -->
                        <div class="form-group row">
                            <label for="mgt_uname" class="col-sm-2">User name</label>
                            <div class="col-sm-10">
                                <input type="text" name="mgt_uname" id="mgt_uname" class="form-control" >
                            </div>
                        </div>

                        
                        <!-- #01 -->
                        <div class="form-group row">
                            <label for="mgt_pass1" class="col-sm-2">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="mgt_pass1" id="mgt_pass1" class="form-control" required >
                            </div>
                        </div>

                        <!-- #02 -->
                        <div class="form-group row">
                            <label for="mgt_pass2" class="col-sm-2">Konfirmasi Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="mgt_pass2" id="mgt_pass2" class="form-control" required >
                            </div>
                        </div>

                        <!-- button -->
                        <div class="form-group row">
                            <label for="frn_submit" class="col-sm-2"><span class='text-danger'>Periksa Data !</span></label>
                            <div class="col-sm-10 text-right px-5">
                                <input type="submit" name="frn_submit" id="frn_submit" class="btn btn-primary" value="Simpan">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
$('#mgt_pass2').on('keyup' , function(){
    let p1 , p2;
    p1 = $('#mgt_pass1').val();
    p2 = $('#mgt_pass2').val();
    if( p2.length == p1.length ){
        if( p2 != p1 ){
            alert('Password Tidak Identik');
            $('#mgt_pass1').val('');
            $('#mgt_pass1').focus();
            $('#mgt_pass2').val('');
        }
    }
})
</script>