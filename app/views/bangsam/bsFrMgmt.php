<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-success">
                    <h3>MANAGEMENT BANK SAMPAH</h3>
                </div>
                <div class="card-body">
                    <form action="<?=BASEURL;?>Bangsam/setMgmt" method="post">
                        <input type="hidden" name="mod" id="mod" value = "<?=$data['mod'];?>">
                        
                        <!-- #01 -->
                        <div class="form-group row">
                            <label for="mgt_userId" class="col-sm-2">User ID</label>
                            <div class="col-sm-10">
                                <input type="text" name="mgt_userId" id="mgt_userId" class="form-control" readonly value = "<?=$data['mgmt']['userId'];?>">
                            </div>
                        </div>

                        <!-- #02 -->
                        <div class="form-group row">
                            <label for="mgt_fname" class="col-sm-2">Nama Lengkap</label>
                            <div class="col-sm-10">
                                <input type="text" name="mgt_fname" id="mgt_fname" class="form-control" maxlength="30" required  value = "<?=$data['mgmt']['mgmtName'];?>">
                            </div>
                        </div>

                        <!-- #03 -->
                        <div class="form-group row">
                            <label for="mgt_uname" class="col-sm-2">Username</label>
                            <div class="col-sm-10">
                                <input type="text" name="mgt_uname" id="mgt_uname" class="form-control" maxlengtt="16" required  value = "<?=$data['mgmt']['mgmtLogin'];?>" >
                            </div>
                        </div>

                        <!-- #04 -->
                        <!--div class="form-group row">
                            <label for="mgt_pass1" class="col-sm-2">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="mgt_pass1" id="mgt_pass1" class="form-control" required >
                            </div>
                        </div>

                         #05 
                        <div class="form-group row">
                            <label for="mgt_pass2" class="col-sm-2">Konfirmasi Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="mgt_pass2" id="mgt_pass2" class="form-control" required >
                            </div>
                        </div> -->

                        <!-- #06 -->
                        <div class="form-group row">
                            <label for="mgt_level" class="col-sm-2">Level Management</label>
                            <div class="col-sm-10">
                                <select name="mgt_level" id="mgt_level" class="form-control">
                                    <option value="teller">Teller</option>
                                    <option value="bangsam">Admin</option>
                                </select>
                            </div>
                        </div>

                        <!-- button -->
                        <div class="form-group row">
                            <label for="frn_submit" class="col-sm-2"><span class='text-danger'>Periksa Data !</span></label>
                            <div class="col-sm-10 text-right px-5">
                                <input type="submit" name="frn_submit" id="frn_submit" class="btn btn-primary" value="Simpan">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>