<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12">
        <div class="card">
          <div class="card-header bg-success">
            <h3>DATA MANAGEMENT</h3>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-2">
                <a class='btn btn-primary' href="<?=BASEURL;?>Bangsam/frMgmt">Staff Baru</a>
              </div>
              <div class="col-sm-4">
                <?php Alert::show(); ?>
              </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>User Id</th>
                        <th>Nama Lengkap</th>
                        <th>Username</th>
                        <th>Level</th>
                        <th>Kontrol</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data['staff'] AS $staff): ?>
                        <tr>
                          <td><?=$staff['userId'];?></td>
                          <td><?=$staff['mgmtName'];?></td>
                          <td><?=$staff['mgmtLogin'];?></td>
                          <td><?= ucfirst($staff['mgmtLevel']);?></td>
                          <td>
                            <a href="<?=BASEURL;?>Bangsam/frMgmt/liru/<?=$staff['userId'];?>">Edit | </a>
                            <a href="#" onClick = tenane("<?=$staff['userId'];?>") >Hapus | </a>
                            <a href="<?=BASEURL;?>Bangsam/pwdrst/<?=$staff['userId'];?>">Reset password</a>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
  function tenane(uid){
    let saestu = confirm('Pensiunkan Staff ?');
    if( saestu == true ){
      window.location="<?=BASEURL;?>Bangsam/retire/"+uid;
    }else{
      window.location="<?=BASEURL;?>Bangsam/notire/";
    }
  }
</script>