<div class="container-fluid">
  <div class="row">
    <div class="col-lg-4">&nbsp;</div>
    <div class="col-lg-4">
      <div class="text-center">
        <h3>BANK SAMPAH</h3>
        <p class="h4">Uji Coba Sistem</p>
        <ul class="list-group">
          <li class="list-group-item"><a href="<?=BASEURL?>Nasab" class="btn btn-success">Halaman Nasabah</a></li>
          <li class="list-group-item"><a href="<?=BASEURL?>Teller" class="btn btn-success">Halaman Teller</a></li>
          <li class="list-group-item"><a href="<?=BASEURL?>Bangsam" class="btn btn-success">Halaman Admin</a></li>
        </ul>
      </div>
    </div>
    <div class="col-lg-4">&nbsp;</div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
