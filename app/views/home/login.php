<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$data['title'];?></title>
    <style>
        .lb-wrapper{
            position:absolute; width: 320px; height: 320px;
            top: 50%; left: 50%; margin-top: -160px; margin-left: -160px;
            border: 2px solid black; padding: 0px; box-sizing: border-box;
        }
        .lb-header{
            margin: 0; padding: 20px 0px; height: 70px; background-color: darkgreen;
            box-sizing: border-box; text-align: center;
        }
        
        .lb-header h4 { margin: 0; padding:0; color: whitesmoke;}
        .lb-body > form { padding: 20px;}
        .lb-body > form > .form-group { 
            padding: 5px 0px; margin-bottom: 5px;
        }
        .lb-body > form > .form-group > .form-control { 
            padding: 10px; width: 100%; box-sizing: border-box;
            font-size: 16px; border-radius: 5px; border: 1px solid gray;
         }

         .lb-body > form > .form-group > button[type='submit']{
             padding: 15px; font-size: 18px; border: none;
             cursor: pointer; background-color: lime; width: 100px;
             text-align: center;
         }

    </style>
</head>
<body>
<?php 
    alert::show(); 
    
    if($_SESSION['blocked'] === true ) {
        echo "Tunggu 10 menit lagi"; 
        exit();
    }
?>
    <div class="lb-wrapper">
        <div class="lb-header">
            <h4>Login Bank Sampah</h4>
        </div>
        <div class="lb-body">
            <form action="<?=BASEURL;?>Home/authuser" method="post">
                <div class="form-group">
                    <input type="text" name="mgt_uname" id="mgt_uname" class="form-control" placeholder="Username">
                </div>
                <div class="form-group">
                    <input type="password" name="mgt_pass1" id="mgt_pass1" class="form-control" placeholder="Password">
                </div>
                <div class="form-group " style="text-align:right;">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </form> 
        </div>
    
    </div>
</body>
</html>