<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-dark text-light">
                    <h4>Data Nasabah</h4>
                </div>
                <div class="card-body" style="padding:5px;">
                    <p style="padding:0; margin:0">
                    <?=$data['nasabah']['namaNasabah'] . ", ". $data['nasabah']['rtrw'] ." ".$data['nasabah']['desa'] ." [" . $data['nasabah']['nomorKTP'] ."]";?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- form trx -->
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-success">
                    <h4>FORM TRANSAKSI</h4>
                </div>
                <div class="card-body">
                    <form action="<?=BASEURL;?>Teller/setTrx" method="post">
                        
                        <div class="form-group">
                            <label for="tll_tgTrx">Tanggal</label>
                            <input type="date" name="tll_tgTrx" id="tll_tgTrx" class="form-control" value = "<?=date('Y-m-d');?>" readonly >
                        </div>
                        
                        <div class="form-group">
                            <label for="tll_nmRekening">Nomor Rekening</label>
                            <input type="text" name="tll_nmRekening" id="tll_nmRekening" class="form-control" value="<?=$data['accNum'];?>" >
                        </div>

                        <div class="form-group">
                            <label for="tll_kdTrx">Jenis Transaksi</label>
                            <select name="tll_kdTrx" id="tll_kdTrx" class="form-control">
                                <option value="001">Setoran</option>
                                <option value="002">Tarik Tunai</option>
                                <option value="003">Transfer Ekstrnal</option>
                                <option value="004">Administrasi</option>
                                <option value="005">Transfer Internal</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="tll_nominal">Jumlah Uang</label>
                            <input type="number" name="tll_nominal" id="tll_nominal" class="form-control" required >
                        </div>

                        <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary">Set Transaksi</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- list top trx -->
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-success">
                    <h4>RIWAYAT TRANSAKSI</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Kode</th>
                                    <th>Jumlah</th>
                                    <th>Saldo</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($data['trxHistory'] AS $trh): ?>
                                <tr>
                                    <td><?=splitTanggal($trh['tanggal']);?></td>
                                    <td><?=$trh['kdTrx'];?></td>
                                    <td class='text-right'><?=number_format($trh['nominal'],2,',','.');?></td>
                                    <td class='text-right'><?=number_format($trh['saldo'],2,',','.');?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready( function(){
    $('#tll_nominal').focus();
})
</script>
<?php

    function splitTanggal($waktu){
        list($tgl,$jam) = explode(" ",$waktu);
        list($t,$b,$h) = explode("-",$tgl);
        return "{$h}/{$b}/{$t}";
    }

?>