<div class="container-fluid">
  <div class="row"><div class="col-sm-12"><?php Alert::show(); ?></div></div>
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header bg-info">
          <h3>Pencarian Nasabah</h3>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
            
              <table class="table table-sm">
                <tbody>
                    <tr><td colspan='2'>Cari Nasabah</td></tr>
                    <tr>
                    <td>
                        <input type="text" name="tlr_namasabah" id="tlr_namasabah" class="form-control" placeholder="Nama Nasabah / Nomor Rekening">
                    </td>
                    <td><button class="btn btn-primary" id="tlr_nasabahseek">Cari</button></td>
                    </tr>
                    
                </tbody>
              </table>
            </div>
            
            <div class="col-md-8">
                <div class="text-center"><h4>Hasil Pencarian Nasabah</h4></div>
                <div class="table-responsive">
                    <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                        <th>Nama Nasabah</th>
                        <th>Alamat</th>
                        <th>Kontrol</th>
                        </tr>
                    </thead>
                    <tbody id="dataNasab"></tbody>
                    </table>
                </div>      
                </div>
            </div>
          
        </div>
      </div>
    </div>  
  </div>

  <!--div class="row">
    <div class="col-md-2">
      <button class="btn btn-success form-control" id="tlr_trxBaru">Transaksi Baru</button>
    </div>
  </div-->

  <div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header bg-info">
                <h4>Rekaman Transaksi</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>Kode Transaksi</th>
                      <th>Tanggal</th>
                      <th>Nomor Rekening</th>
                      <th>Jumlah</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach($data['toptrx'] AS $ltrx): ?>
                    <tr>
                      <td><?=$ltrx['kdTrx'];?></td>
                      <td><?=olahTanggal($ltrx['tanggal']);?></td>
                      <td><?=$ltrx['nomorRekening'];?></td>
                      <td class='text-right'><?=number_format($ltrx['nominal'],2,',','.');?></td>
                    </tr>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
      <div class="card">
        <div class="card-header bg-info">
          <h4>Permintaan Transfer</h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Nomor Rekening</th>
                  <th>Jumlah</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>
              <?php foreach($data['rqtrfr'] as $reqtrf ): ?>
                <tr>
                  <td><?=olahTanggal($reqtrf['tanggal']);?></td>
                  <td><?=$reqtrf['nomorRekening'];?></td>
                  <td class='text-right'><?=number_format($reqtrf['nominal'],2,',','.');?></td>
                  <td>
                    <a href="<?=BASEURL;?>Teller/setEtfr/<?=$reqtrf['idx_permintaan'];?>">
                    <?=$reqtrf['trfStatus'];?>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script src="<?=BASEURL;?>js/bsconfig.js"></script>
<script src="<?=BASEURL;?>js/teller-main.js"></script>
<?php
  function olahTanggal($waktu){
    list($tgl,$jam) = explode(" ",$waktu);
    list($t,$b,$h) = explode("-",$tgl);
    return("{$h}/{$b}/{$t}/ {$jam}");
  }
?>