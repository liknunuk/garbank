<?php
class Model_user
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function authuser($data){
        $kerupuk = [];
        $managmt = $this->isman($data);
        $nasabah = $this->isnas($data);
        if( $managmt != NULL ){
            $authData = ['tipe'=>'management' , 'data' => $managmt ];
            return $authData;
        }elseif( $nasabah != NULL ){
            $authData = ['tipe'=>'nasabah' , 'data' => $nasabah ];
            return $authData;
        }else{
            $uip = $this->getUserIpAddr();
            $authData = ['tipe'=>'kerupuk' , 'data' => $uip ];
            return $authData;
        }
    }
    private function isman($data){
        $vakey = md5("(".$data['mgt_uname']."_".$data['mgt_pass1'].")");
        $sql = "SELECT * FROM mgmtUser WHERE mgmtVaKey = :vakey LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('vakey',$vakey);
        $this->db->execute();
        return $this->db->resultOne();
    }

    private function isnas($data){
        $nmPIN = md5($data['mgt_uname']."_".$data['mgt_pass1']);
        $sql = "SELECT * FROM bs_nasabah WHERE nomorPin = :nmPIN LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('nmPIN',$nmPIN);
        $this->db->execute();
        return $this->db->resultOne();
    }

    private function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
