<?php
class Model_bangsam
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    // tambahNasabah
    public function tambahNasabah($data){
        $nomorPin = $data['frn_nmRekening']."_".$data['frn_pin'];
        $sql = "INSERT INTO bs_nasabah SET namaNasabah = :nama , nomorKTP  = :ktp , rtrw = :rtrw , desa = :desa , nomorTelp = :telp , bankTransfer = :banktrx ,rekeningTransfer = :akuntrx , nomorPin = :pin ,nomorRekening = :nomorek";
        $this->db->query($sql);
        $this->db->bind('nama',$data['frn_namalenkap']);
        $this->db->bind('ktp',$data['frn_nik']);
        $this->db->bind('rtrw',$data['frn_rtrw']);
        $this->db->bind('desa',$data['frn_desa']);
        $this->db->bind('telp',$data['frn_telp']);
        $this->db->bind('banktrx',$data['frn_banktrf']);
        $this->db->bind('akuntrx',$data['frn_accntrf']);
        $this->db->bind('pin',md5($nomorPin));
        $this->db->bind('nomorek',$data['frn_nmRekening']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // updateNasabah
    public function updateNasabah($data){
        $sql = "UPDATE bs_nasabah SET namaNasabah = :nama , nomorKTP  = :ktp , rtrw = :rtrw , desa = :desa , nomorTelp = :telp , bankTransfer = :banktrx ,rekeningTransfer = :akuntrx  WHERE nomorRekening = :nomorek";
        $this->db->query($sql);
        $this->db->bind('nama',$data['frn_namalenkap']);
        $this->db->bind('ktp',$data['frn_nik']);
        $this->db->bind('rtrw',$data['frn_rtrw']);
        $this->db->bind('desa',$data['frn_desa']);
        $this->db->bind('telp',$data['frn_telp']);
        $this->db->bind('banktrx',$data['frn_banktrf']);
        $this->db->bind('akuntrx',$data['frn_accntrf']);
        $this->db->bind('nomorek',$data['frn_nmRekening']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // updatePinNasabah
    public function updatePinNasabah($data){
        $sql = "UPDATE bs_nasabah SET nomorPin = :pin  WHERE nomorRekening = :nomorek";
        $this->db->query($sql);
        
        $this->db->bind('pin',$data['frn_pin']);
        $this->db->bind('nomorek',$data['frn_nmRekening']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // daftar nasabah
    public function listNasabah(){
        $sql = "SELECT * FROM bs_nasabah ORDER BY right(nomorRekening,8) DESC LIMIT ". rows;
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // data nasabah
    public function dataNasabah($data){
        $sql = "SELECT bs_nasabah.* , bankTrx.namaBank FROM bs_nasabah , bankTrx WHERE nomorRekening =:nomorek && bankTrx.kodeBank = bs_nasabah.bankTransfer";
        $this->db->query($sql);
        $this->db->bind('nomorek',$data['frn_nmRekening']);
        $this->db->execute();
        return $this->db->resultOne();
    }

    // inaktifNasabah
    public function deactivateNasabah($data){
        $sql = "UPDATE bs_nasabah SET statusAktif = 'inaktif'  WHERE nomorRekening = :nomorek";
        $this->db->query($sql);
        
        $this->db->bind('nomorek',$data['frn_nmRekening']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // tambahUser
    public function tambahUser($data){
        $password = md5("(" . $data['mgt_uname'] . "300965)");
        $sql = "INSERT INTO mgmtUser SET mgmtName = :nama , mgmtLogin = :user , mgmtVaKey = :vakey , mgmtLevel = :ulevel";
        $this->db->query($sql);
        $this->db->bind('nama',$data['mgt_fname']);
        $this->db->bind('user',$data['mgt_uname']);
        $this->db->bind('vakey',$password);
        $this->db->bind('ulevel',$data['mgt_level']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // updateUser
    public function updateUser($data){
        $password = md5("(" . $data['mgt_uname'] . "_300965)");
        $sql = "UPDATE mgmtUser SET mgmtName = :nama , mgmtLogin = :user , mgmtLevel = :ulevel, mgmtVaKey = :vakey WHERE userId = :userId ";
        $this->db->query($sql);
        $this->db->bind('nama',$data['mgt_fname']);
        $this->db->bind('user',$data['mgt_uname']);
        $this->db->bind('ulevel',$data['mgt_level']);
        $this->db->bind('vakey',$password);
        $this->db->bind('userId',$data['mgt_userId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // update user password
    public function updateUserPassword($data){
        $password = md5("(" . $data['mgt_uname'] .'_'. $data['mgt_pass1']. ")");
        $sql = "UPDATE mgmtUser SET  mgmtVaKey = :vakey WHERE userId = :userId ";
        $this->db->query($sql);
        $this->db->bind('vakey',$password);
        $this->db->bind('userId',$data['mgt_userId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // daftar User
    public function listUser(){
        $sql = "SELECT * FROM mgmtUser ORDER BY mgmtName LIMIT " . rows;
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // data User
    public function dataUser($uid){
        $sql = "SELECT * FROM mgmtUser WHERE userId = :usid ";
        $this->db->query($sql);
        $this->db->bind('usid' , $uid);
        $this->db->execute();
        return $this->db->resultOne();
    }

    // reset user password
    public function pwdrst($uid){
        
        $data = $this->getUname($uid);
        // echo $data['mgmtLogin'];
        $password = md5("(" . $data['mgmtLogin'] . "_300965)");
        
        $sql  = "UPDATE mgmtUser SET mgmtVaKey = :vakey WHERE userId = :userId LIMIT 1";

        $this->db->query($sql);
        $this->db->bind('vakey',$password);
        $this->db->bind('userId',$uid);
        $this->db->execute();
        return $this->db->rowCount();
    }

    private function getUname($uid){
        $this->db->query("SELECT mgmtLogin FROM mgmtUser WHERE userId = :userId LIMIT 1");
        $this->db->bind('userId',$uid);
        $this->db->execute();
        return $this->db->resultOne();
    }

    // inaktifUser
    public function deactivateUser($uid){
        $sql = "DELETE FROM mgmtUser WHERE userId = :userId";
        $this->db->query($sql);
        $this->db->bind('userId' , $uid);
        $this->db->execute();
        return $this->db->rowCount();

    }

    // login
    public function userauth($data){
        $vakey = md5("(" . $data['mgt_uname'] . $data['mgt_pass1'] . ")");
        $sql = "SELECT * FROM mgmtUser WHERE mgmtVaKey = :vakey && mgmtLevel = 'bangsam'";
        $this->db->query($sql);
        $this->db->bind('vakey',$vakey);
        if( $this->db->rowCount() > 0){
            return $this->db->resultOne();
        }else{
            return NULL;
        }
    }

    public function dtKecamatan(){
        $sql = "SELECT * FROM kodeKecamatan";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function nmRekBaru($prfx){
        $sql = "SELECT MAX(RIGHT(nomorRekening,5)) nomorTerakhir FROM bs_nasabah WHERE nomorRekening LIKE :prfx";
        $this->db->query($sql);
        $this->db->bind('prfx',$prfx . "%" );
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function bankList(){
        $sql = "SELECT * FROM bankTrx";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // total akumulasi saldo
    public function totalSaldo(){
        $sql = "SELECT SUM(saldo) totalSaldo FROM vSaldo";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultOne();
    }

    // total akumulasi saldo
    public function nasabahAktif(){
        $sql = "SELECT COUNT(nomorRekening) aktif FROM bs_nasabah WHERE statusAktif='aktif'";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultOne();
    }
     
}
