<?php
class Model_teller
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    // 40 transaksi terakhir
    public function last40trx(){
        $sql = "SELECT * FROM ( SELECT * FROM bs_transaksi ORDER BY idx_trx DESC LIMIT " . rows." ) last40 ORDER BY idx_trx ASC";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // catat transaksi
    public function trxRecord($data){
        // generate kode transaksi
        $trxId = $this->newTrxNumber();
        
        // cari saldo terakhir ybs
        $saldoAkhir = $this->cariSaldo($data['tll_nmRekening']);
        $sa = ( $saldoAkhir['saldo'] == NULL || $saldoAkhir == 0 ) ? 0 : $saldoAkhir['saldo'];

        // tentukan saldo bertambah atau berkurang
        $saldo = $this->hitungSaldo($data['tll_kdTrx'],$data['tll_nominal'],$sa);

        

        // $sql = "INSERT INTO bs_transaksi SET kdTrx = :kode , nomorRekening  = :nomor , tanggal  = :tanggal , nominal  = :nominal , saldo = :saldo , idx_trx = :trxId";
        $sql = "INSERT INTO bs_transaksi SET kdTrx = :kode , nomorRekening  = :nomor ,  nominal  = :nominal , saldo = :saldo , idx_trx = :trxId";
        $this->db->query($sql);

        $this->db->bind('kode',     $data['tll_kdTrx']);
        $this->db->bind('nomor',    $data['tll_nmRekening']);
        // $this->db->bind('tanggal',  $data['tll_tgTrx']);
        $this->db->bind('nominal',  $data['tll_nominal']);
        $this->db->bind('trxId',    $trxId);
        $this->db->bind('saldo',    $saldo);
        
        $this->db->execute();
        return $this->db->rowCount();
    }

    // cari saldo
    private function cariSaldo($rek){
        // cari saldo dari index transaksi terakhir.
        $sql = "SELECT saldo FROM bs_transaksi WHERE nomorRekening = :norek ORDER BY idx_trx DESC LIMIT 1 ";
        $this->db->query($sql);
        $this->db->bind('norek',$rek);
        $this->db->execute();
        return $this->db->resultOne();
    }

    // hitung saldo akhir
    private function hitungSaldo($kdtrx , $nominal , $sa){
        switch($kdtrx){
            case '001' : $sr = $sa + $nominal; break;
            case '002' : $this->saldoTarik($sa,$nominal); $sr = $sa - $nominal; break;
            case '003' : $this->saldoTarik($sa,$nominal); $sr = $sa - ( $nominal + 10000 ); break;
            case '004' : $sr = $sa - $nominal; break;
            case '005' : $this->saldoTarik($sa,$nominal); $sr = $sa - ( $nominal + 10000 ); break;
        }
        return $sr;
    }

    // cek ketentuan tarik tunai / transfer
    private function saldoTarik($sa,$nominal){
        if( $sa < 10000 ){
            Alert::set('Saldo','kurang dari','batas minimal','danger');
            Header("Location:" . BASEURL . "Teller/");
            exit();
        }else{
            if( $sa < ( $nominal + 10000) ){
                Alert::set('Saldo akhir','tidak','mencukupi','danger');
                Header("Location:" . BASEURL . "Teller/");
                exit();
            }else{
                if( $nominal < 10000 ){
                    Alert::set('Nominal','kurang dari','jumlah minimal','danger');
                    Header("Location:" . BASEURL . "Teller/");
                    exit();
                }
            } 
        }
    }


    // riwayat transaksi
    public function trxHistory($accNum){
        // $sql = "SELECT * FROM bs_transaksi WHERE nomorRekening = :rekening ORDER BY idx_trx DESC LIMIT " . rows;
        $sql = "SELECT * FROM ( SELECT * FROM bs_transaksi WHERE nomorRekening = :rekening ORDER BY idx_trx DESC LIMIT " . rows . " ) sub ORDER BY  idx_trx ASC";
        $this->db->query($sql);
        $this->db->bind('rekening' , $accNum);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // cek permintaan transfer
    public function cekRqTrx(){
        $sql = " SELECT * FROM bs_mintaTransfer WHERE trfStatus = 'Tunda' ORDER BY idx_permintaan LIMIT " . rows;
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // cari nasabah
    public function cariNasab($data){
        $nama = "%" . $data['tlr_namasabah'] . "%";
        $nork = $data['tlr_namasabah'];
        $sql = "SELECT * FROM bs_nasabah WHERE namaNasabah LIKE :nama OR nomorRekening LIKE :noRek LIMIT 20";
        $this->db->query($sql);
        $this->db->bind('nama',$nama);
        $this->db->bind('noRek',"%" . $nork);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // login
    public function tlAuth($data){
        $vakey = md5("(" . $data['mgt_uname'] . $data['mgt_pass1'] . ")");
        $sql = "SELECT * FROM mgmtUser WHERE mgmtVaKey = :vakey && mgmtLevel = 'teller'";
        $this->db->query($sql);
        $this->db->bind('vakey',$vakey);
        if( $this->db->rowCount() > 0){
            return $this->db->resultOne();
        }else{
            return NULL;
        }
    }

    private function newTrxNumber(){
        $thisYear = date('Y');
        $sql = "SELECT MAX(idx_trx) id FROM bs_transaksi WHERE idx_trx LIKE :tahun";
        $this->db->query($sql);
        $this->db->bind('tahun' , $thisYear."%");
        $this->db->execute();
        $trx = $this->db->resultOne();

        if($trx['id'] == NULL ){
            $trxId = $thisYear . sprintf("%06d",1);
        }else{
            $trxId = $trx['id'] + 1;
        }

        return $trxId;
    }

    public function setEtfr($idx){

        // Ambil Data Permintaan transfer
        $rqdata = $this->getEtfrData($idx);
        $accnum = $rqdata['nomorRekening'];
        $jumlah = $rqdata['nominal'];
        
        // Catat Pengeluaran, kode trx 003
        $etrfr1 = ['tll_kdTrx'=>'003','tll_nmRekening'=>$accnum,'tll_nominal'=>$jumlah];
        if( $this->trxRecord($etrfr1) == 0 ){
            echo "Gagal Catat Mutasi 003";
            exit();
        }
        
        // Catat Bea Transfer, kode trx 004 Rp 5000
        $etrfr2 = ['tll_kdTrx'=>'004','tll_nmRekening'=>$accnum,'tll_nominal'=>5000];
        if( $this->trxRecord($etrfr2) == 0 ){
            echo "Gagal Catat Mutasi 004";
            exit();
        }
    
        // Ubah Status Permintaan
        $sql = "UPDATE bs_mintaTransfer SET trfStatus = 'Selesai' WHERE idx_permintaan = :idx";
        $this->db->query($sql);
        $this->db->bind('idx' , $idx );
        $this->db->execute();
        return $this->db->rowCount();
      }
    
      private function getEtfrData($idx){
        $sql = "SELECT nomorRekening, nominal  FROM bs_mintaTransfer WHERE idx_permintaan = :idRqst";
        $this->db->query($sql);
        $this->db->bind('idRqst' , $idx);
        $this->db->execute();
        return $this->db->resultOne();
      }

      public function setNewPassword($data){
        $iam = $this->isItMe($data['mgt_userId']);
        if( $iam['mgmtLogin'] == $data['mgt_uname']){
            $password = md5("(" . $data['mgt_uname'] .'_'. $data['mgt_pass1']. ")");
            $sql = "UPDATE mgmtUser SET  mgmtVaKey = :vakey WHERE userId = :userId ";
            $this->db->query($sql);
            $this->db->bind('vakey',$password);
            $this->db->bind('userId',$data['mgt_userId']);
            $this->db->execute();
            return $this->db->rowCount();
        }else{
            return 0;
        }
      }

      private function isItMe($uid){
          $sql = "SELECT mgmtLogin FROM mgmtUser WHERE userId = :userId LIMIT 1";
          $this->db->query($sql);
          $this->db->bind('userId' , $uid);
          $this->db->execute();
          return $this->db->resultOne();
      }

}