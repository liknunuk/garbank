<?php
class Model_nasabah
{
    private $table = "nama tabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    // catat lihat tabungan
    public function cekSaving($accNum,$pn=1){
        $hal = ( $pn - 1 ) * rows;
        $sql = "SELECT * FROM bs_transaksi WHERE nomorRekening = :norek ORDER BY idx_trx DESC LIMIT  $hal , ".rows;
        $this->db->query($sql);
        $this->db->bind('norek',$accNum);
        $this->db->execute();
        return $this->db->resultSet();
    }

    // permintaan transfer
    public function rqTrx($accNum,$amount){
        $idtrfr = $this->idxRqTfr();
        $sql = "INSERT INTO bs_mintaTransfer SET nomorRekening = :norek , nominal = :nomin , idx_permintaan = :rqidx";
        $this->db->query($sql);
        $this->db->bind("norek" , $accNum);
        $this->db->bind("nomin" , $amount);
        $this->db->bind("rqidx" , $idtrfr);
        $this->db->execute();
        return $this->db->rowCount();
    }

    private function idxRqTfr(){
        $thisYear = date('Y');
        $sql = "SELECT MAX(idx_permintaan) id FROM bs_mintaTransfer WHERE idx_permintaan LIKE :tahun";
        $this->db->query($sql);
        $this->db->bind('tahun' , $thisYear."%");
        $this->db->execute();
        $trx = $this->db->resultOne();

        if($trx['id'] == NULL ){
            $trxId = $thisYear . sprintf("%06d",1);
        }else{
            $trxId = $trx['id'] + 1;
        }

        return $trxId;
    }

    // login
    public function userauth(){}

    public function kodeTrx(){
        $sql = "SELECT * FROM kodeTrx";
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
    }

    public function saldoAkhir($accNum){
        $sql = "SELECT * FROM vSaldo WHERE nomorRekening = :accNum";
        $this->db->query($sql);
        $this->db->bind("accNum" , $accNum);
        $this->db->execute();
        return $this->db->resultOne();
    }

}
