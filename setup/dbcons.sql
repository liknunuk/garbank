-- data referensi
-- nama kecamatan
DROP TABLE IF EXISTS kodeKecamatan;
CREATE TABLE kodeKecamatan(
    nomorKecamatan int(3) not null,
    namaKecamatan varchar(30),
    primary key (nomorKecamatan)
);

-- dump kodeKecamatan
INSERT INTO kodeKecamatan VALUES
(104,'Banjarmangu'),
(105,'Banjarnegara'),
(106,'Batur'),
(107,'Bawang'),
(108,'Kalibening'),
(109,'Karangkobar'),
(110,'Madukara'),
(111,'Mandiraja'),
(112,'Pagedhongan'),
(113,'Pagentan'),
(114,'Pandanarum'),
(115,'Pejawaran'),
(116,'Punggelan'),
(117,'Purwanegara'),
(118,'Purworejo-Klampok'),
(119,'Rakit'),
(120,'Sigaluh'),
(121,'Susukan'),
(122,'Wanadadi'),
(123,'Wanayasa');

-- nama bank transfer
DROP TABLE IF EXISTS bankTrx;
CREATE TABLE bankTrx(
    kodeBank int(3) not null auto_increment,
    namaBank varchar(30),
    primary key (kodeBank)
) ENGINE = innoDB;

-- dump data bank transfer
INSERT INTO bankTrx VALUES
(001,  'Tanpa Bank'),
(002 , 'Bank BRI'),
(003 , 'Bank BNI 46'),
(004 , 'Bank MANDIRI'),
(005 , 'Bank BTN'),
(006 , 'Bank BTPN'),
(007 , 'Bank MANDIRI SYARIAH'),
(008 , 'Bank BCA'),
(009 , 'Bank SURYA YUDHA'),
(010 , 'Bank DANAMON');

-- kode transaksi
DROP TABLE IF EXISTS kodeTrx;
CREATE TABLE kodeTrx(
    kode varchar(3) not null,
    arti varchar(30) not null,
    primary key (kode)
) ENGINE = innoDB;

-- dump kodeTrx
INSERT INTO kodeTrx VALUES
('001', 'Setoran'),
('002', 'Tarik Tunai'),
('003', 'Transfer Eksternal'),
('004', 'Administrasi'),
('005', 'Transfer Internal');

-- data nasabah
DROP TABLE IF EXISTS bs_nasabah;
CREATE TABLE bs_nasabah(
    nomorRekening varchar(14) NOT NULL, -- kdKecamatan-thReg-nmUrut 
    namaNasabah varchar(30) NOT NULL,
    nomorKTP varchar(16) NOT NULL,
    rtrw varchar(5),
    desa varchar(25),
    nomorTelp varchar(16),
    bankTransfer int(3),
    rekeningTransfer varchar(30),
    statusAktif enum('aktif','inaktif') default 'aktif',
    nomorPin varchar(32) not null,
    primary key (nomorRekening),
    CONSTRAINT kt_bankTrx 
        FOREIGN KEY (bankTransfer) REFERENCES bankTrx(kodeBank)
        ON DELETE CASCADE
		ON UPDATE RESTRICT
) ENGINE = innoDB;

DROP TABLE IF EXISTS bs_transaksi;
CREATE TABLE bs_transaksi(
    idx_trx int(10) unsigned not null, -- tahun(4)nomorurut(6)
    kdTrx varchar(3) not null,
    nomorRekening varchar(14) NOT NULL,
    tanggal timestamp DEFAULT CURRENT_TIMESTAMP(),
    nominal int(11) UNSIGNED NOT NULL DEFAULT 0,
    saldo int(11) UNSIGNED NOT NULL,
    CONSTRAINT kt_kodeTrx
        FOREIGN KEY (kdTrx) REFERENCES kodeTrx(kode)
        ON DELETE CASCADE
		ON UPDATE RESTRICT,
    CONSTRAINT kt_trxRkNasabah
        FOREIGN KEY (nomorRekening) REFERENCES bs_nasabah(nomorRekening)
        ON DELETE CASCADE
		ON UPDATE RESTRICT
) ENGINE = innoDB;

DROP TABLE IF EXISTS indexHargaSampah;
CREATE TABLE indexHargaSampah(
    idx int(4) unsigned not null auto_increment,
    namaBarang varchar(30) not null,
    satuan varchar(10),
    harga int(5) unsigned not null default 0,
    primary key(idx)
);

DROP TABLE IF EXISTS bs_mintaTransfer;
CREATE TABLE bs_mintaTransfer(
    idx_permintaan int(10) UNSIGNED NOT NULL, -- tahun(4)nomorurut(6)
    nomorRekening varchar(14) NOT NULL,
    tanggal timestamp DEFAULT CURRENT_TIMESTAMP(),
    nominal int(11) UNSIGNED NOT NULL DEFAULT 0,
    trfStatus enum('Tunda','Selesai') default 'Tunda',
    primary key (idx_permintaan),
    CONSTRAINT kt_trfRkNasabah
    FOREIGN KEY (nomorRekening) REFERENCES bs_nasabah(nomorRekening)
    ON DELETE CASCADE
    ON UPDATE RESTRICT
) ENGINE = innoDB;

DROP TABLE IF EXISTS mgmtUser;
CREATE TABLE mgmtUser(
    userId int(3) UNSIGNED NOT NULL auto_increment,
    mgmtName varchar(30) NOT NULL,
    mgmtLogin varchar(16) NOT NULL,
    mgmtVaKey varchar(32) NOT NULL,
    mgmtLevel enum('bangsam','teller') default 'teller',
    primary key(userId)
);

-- INIT USER
INSERT INTO mgmtUser VALUES('' , 'Administratur' , 'amirulsam',md5('(amirulsam_anarupiah)'),'bangsam');

-- DROP TABLE IF EXISTS tabel;
-- CREATE TABLE tabel();

-- VIEWS
CREATE OR REPLACE VIEW vSaldo AS SELECT a.idx_trx , a.nomorRekening, a.saldo FROM bs_transaksi a WHERE idx_trx = ( SELECT MAX(b.idx_trx) FROM bs_transaksi b WHERE b.nomorRekening = a.nomorRekening)
