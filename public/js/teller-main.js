$(document).ready( function(){

    $('#tlr_namasabah').focus();
    
    // cari nasabah by nama / nomor rekening
    $('#tlr_nasabahseek').click( function(){
        let data = $('#tlr_namasabah').val();
        cariNasabah(data);  
    })

    $('#tlr_namasabah').keydown( function(e){
        if( e.which == 13 ){
            e.preventDefault();
            cariNasabah( $(this).val() );
        }
    })
})

function cariNasabah(kw){
    $.getJSON( datasource + `tl_carinas/${kw}` , function(nasabah){
        $('#dataNasab tr').remove();
        $.each( nasabah , function(i,data){
            $('#dataNasab').append(`
            <tr>
            <td>${data.namaNasabah}</td>
            <td>${data.rtrw} ${data.desa}</td>
            <td><a href='${homesource}Teller/trx/${data.nomorRekening}'>Transaksi</a></td>
            <tr>
            `)
        })
    })
}