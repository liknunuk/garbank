$(document).ready( function(){

    $('#dfn_namasabah').focus();
    
    // cari nasabah by nama / nomor rekening
    $('#dfn_nasabahseek').click( function(){
        let data = $('#dfn_namasabah').val();
        cariNasabah(data);  
    })

    $('#dfn_namasabah').keydown( function(e){
        if( e.which == 13 ){
            e.preventDefault();
            cariNasabah( $(this).val() );
        }
    })
})

function cariNasabah(kw){
    $.getJSON( datasource + `tl_carinas/${kw}` , function(nasabah){
        $('#dataNasab tr').remove();
        $.each( nasabah , function(i,data){
            $('#dataNasab').append(`
            <tr>
            <td>${data.nomorRekening}</td>
            <td>${data.namaNasabah}</td>
            <td>RT/RW: ${data.rtrw}, ${data.desa}</td>
            <td>${data.nomorKTP}</td>
            <td>${data.nomorTelp}</td>
            <td>
                <a href=`+homesource+`Bangsam/dtNasabah/${data.nomorRekening}">Detil</a>
            </td>
            </tr>
            `)
        })
    })
}